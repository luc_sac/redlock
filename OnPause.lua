local composer = require "composer"
local utility = require "loadsave"
local MyData = require "mydata"
local widget = require "widget"
local EsciSolverInCorso
local utility = require "loadsave"

local scene = composer.newScene()
local current = composer.getSceneName( "current" )
local LivelloDoveSono, FlagScelta,ArraySpriteArrowPause,ArraySpriteParentesiVero

function scene:create( event )
  print( "CREATAONPAUSE" )
  MyData.settings =  utility.loadTable("settings9.json")
  EsciSolverInCorso = event.params.inResolve
  local Preced = composer.getSceneName( "previous" )
  print( Preced )
  ArraySpriteArrowPause = event.params.ArraySpritePause
  ArraySpriteParentesiVero = event.params.ArraySpriteParentesi

	FlagScelta = true
	local sceneGroup = self.view

	local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
    	local YscaleFactor = fitHeight / displayObject.height 
    	local XscaleFactor = fitWidth / displayObject.width 
    	displayObject:scale( XscaleFactor, YscaleFactor )
  	end

  local Oscure = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight )
 	Oscure.alpha = 0.75
  Oscure:setFillColor( 0,0,0 )

  local GridPopUp = display.newImage( "GridPauseTest.png")
  GridPopUp.x = display.contentCenterX
  GridPopUp.y = display.contentCenterY
  fitImage(GridPopUp,event.params.checkWidth-10, event.params.checkHeight/1.2)
  sceneGroup:insert( GridPopUp )

  local gbOffsetX = GridPopUp.x - ( GridPopUp.width * GridPopUp.anchorX )
  local xOffset = gbOffsetX + (GridPopUp.width/3)

  --MyData.settings = utility.loadTable("settings9.json")
	 
  local TextPopUp1 = display.newText(sceneGroup, "Pause", display.contentCenterX, display.contentCenterY -96 , "VAG.ttf",22 )
  TextPopUp1:setFillColor(1,1,1 )

  local optz1 = {
    width = 1022,
    height = 192,
    defaultFile = "OnPauseResumeBot.png",
    overFile = "OnPauseResumeBotOver.png",
    onEvent = LanciaResume,
    isEnabled = true
  }
  local optz2 = {
    width = 1022,
    height = 192,
    defaultFile = "OnPauseResumeBot.png",
    overFile = "OnPauseResumeBotOver.png",
    onEvent = LanciaGoAllBoard,
    isEnabled = true
  }
  local optz3 = {
    width = 1022,
    height = 192,
    defaultFile = "OnPauseResumeBot.png",
    overFile = "OnPauseResumeBotOver.png",
    onEvent = LanciaExit,
    isEnabled = true
  }

  local ResumeBot = widget.newButton( optz1 )
 	ResumeBot.x = display.contentCenterX
 	ResumeBot.y = display.contentCenterY-22
 	ResumeBot.xScale = 0.166
  ResumeBot.yScale =  0.182
 	sceneGroup:insert( ResumeBot )
 	local TextResume = display.newText( sceneGroup, "Resume", ResumeBot.x, ResumeBot.y, "VAG.ttf",18 )


 	local PuzzleBot = widget.newButton( optz2 )
 	PuzzleBot.x = display.contentCenterX
 	PuzzleBot.y = display.contentCenterY+23
 	PuzzleBot.xScale = 0.166
  PuzzleBot.yScale =  0.182
 	sceneGroup:insert( PuzzleBot )
 	local TextPuzzle = display.newText( sceneGroup, "Boards", PuzzleBot.x, PuzzleBot.y, "VAG.ttf",18 )

 	local ExitBot = widget.newButton( optz3 )
 	ExitBot.x = display.contentCenterX
 	ExitBot.y = display.contentCenterY+68
 	ExitBot.xScale = 0.166
  ExitBot.yScale =  0.182
 	sceneGroup:insert( ExitBot)
 	local TextExit = display.newText( sceneGroup, "Exit", ExitBot.x, ExitBot.y, "VAG.ttf",18 )

 	LivelloDoveSono = event.params.level


    function ExitTap( event )
      if EsciSolverInCorso then
        MyData.settings.hint =MyData.settings.hint-1
      end 
      utility.saveTable(MyData.settings, "settings9.json")
      FlagScelta = false
      composer.hideOverlay( )
      for indiceLocaleChangeBoardSX=1,#ArraySpriteArrowPause do
        transition.cancel( ArraySpriteArrowPause[indiceLocaleChangeBoardSX])  
      end
      composer.removeScene( "gameBeginner")
      composer.gotoScene( "Home", {effect = "fade"} )
      return true
    end


  	function ResumeListener(event)
      utility.saveTable(MyData.settings, "settings9.json")
  		composer.hideOverlay("slideLeft")
  		--composer.removeScene( current)


  	end

  	function ToRestart(event )
  		FlagScelta = false
      utility.saveTable(MyData.settings, "settings9.json")
  		composer.hideOverlay()
      for indiceLocaleChangeBoardSX=1,#ArraySpriteArrowPause do
        transition.cancel( ArraySpriteArrowPause[indiceLocaleChangeBoardSX])
      end
  		composer.removeScene( "gameBeginner" )
      composer.gotoScene( "gameBeginner", { params = {id = LivelloDoveSono } } )
        --composer.removeScene( current )
      return true 
  	end

  	function GoAllBoard(event)
  		FlagScelta= false
      if EsciSolverInCorso then
        MyData.settings.hint =MyData.settings.hint-1
      end 
      utility.saveTable(MyData.settings, "settings9.json")
  		composer.hideOverlay()
      --for indiceCicloSpriteParentesi = 1,
      for indiceCicloArrayParentesi = 1,#ArraySpriteParentesiVero do
        transition.cancel( ArraySpriteParentesiVero[indiceCicloArrayParentesi])
        display.remove(ArraySpriteParentesiVero[indiceCicloArrayParentesi])
        ArraySpriteParentesiVero[indiceCicloArrayParentesi] = nil

      end
      for indiceLocaleChangeBoardSX=1,#ArraySpriteArrowPause do
        transition.cancel( ArraySpriteArrowPause[indiceLocaleChangeBoardSX])
        display.remove(ArraySpriteArrowPause[indiceLocaleChangeBoardSX])
        ArraySpriteArrowPause[indiceLocaleChangeBoardSX] = nil 
      end
      --composer.removeScene( "gameBeginner")
     	composer.gotoScene( "level_selection",{effect = "zoomInOutFade", time = 500})
      return true

  	end


  	ResumeBot:addEventListener( "tap", ResumeListener )
  	PuzzleBot:addEventListener( "tap", GoAllBoard )
    ExitBot:addEventListener( "tap", ExitTap )







end

function LanciaResume(event)
  if event.phase == "ended" or event.phase == "cancelled" then
    ResumeListener()
  end 
end

function LanciaExit(event)
  if event.phase == "ended" or event.phase == "cancelled" then
    ExitTap()
  end
end

function LanciaGoAllBoard(event)
  if event.phase == "ended" or event.phase == "cancelled" then
    GoAllBoard()
  end
end





function scene:show(event)
	print( "SHOW!!" )
  local phase = event.phase
  if ( phase == "will" ) then
    --Runtime:addEventListener("enterFrame", enterFrame)
  elseif ( phase == "did" ) then

  end
end

function scene:hide( event )
	print("HIDE!!!")
	local parent = event.parent
	local phase = event.phase
		if ( phase == "will" ) then
			if FlagScelta then
				print( "FlagScelta--->" )
				print( FlagScelta )
				parent:ProvaEnterFrame()
			else
				print( "FlagScelta FALSe" )
			end
		elseif ( phase == "did" ) then
    
      --print("HIDE: "..hide)
  		end
end

function scene:destroy( event )
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene