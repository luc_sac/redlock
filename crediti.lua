local composer = require( "composer" )
local widget = require "widget"
local utility = require "loadsave"
local MyData = require "mydata"
local TitoloLock,TitoloRed ,Play,Boards,SettingsHome
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 
 
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view

    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end

   

    local background = display.newImage("Large.jpg")
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    fitImage(background,display.actualContentWidth,display.actualContentHeight)
    sceneGroup:insert( background )
    
    local Header = display.newImage("HeaderSelectionLevel.png" ) 
    Header.x = display.contentCenterX
    Header.y = display.safeScreenOriginY+30
    fitImage(Header,display.actualContentWidth,60)
    sceneGroup:insert( Header )
    
    local TestoHeader = display.newText( sceneGroup, "Credits", display.contentCenterX, Header.y, "VAG.ttf",34 )

    local testoIlaria = display.newText( sceneGroup, "Ilaria Introini", display.contentCenterX, display.contentCenterY-60,"VAg.ttf",23 )
    local testoIlariaVersLunga = display.newText( sceneGroup, "(Grafica e Programmazione) ", display.contentCenterX, testoIlaria.y+20,"VAg.ttf",19 )

    local testoLuca = display.newText( sceneGroup, "Luca Bartolomeo Sacco", display.contentCenterX, display.contentCenterY+40,"VAg.ttf",23 )
    local testoLucaVersLunga = display.newText( sceneGroup, "(Programmazione) ", display.contentCenterX, testoLuca.y+20,"VAg.ttf",19 )
    
    local optzTB = {
        width = 626,
        height = 626,
        defaultFile = "TurnBack.png",
        overFile = "TurnBackOver.png",
        isEnabled = true
     }



    local TurnBack = widget.newButton( optzTB )
    TurnBack.x = 20
    TurnBack.y = Header.y
    fitImage(TurnBack,25,25)
    sceneGroup:insert( TurnBack )

    function TurnBackListener(event) 
        composer.gotoScene( "Home", {effect = "slideRight"} )
    end


    --[[local TitoloBlock = display.newImage( "BottoneTitolo.png")
    TitoloBlock.y = display.screenOriginY+100
    TitoloBlock.x = display.contentCenterX
    fitImage(TitoloBlock,250,120)
    sceneGroup:insert(TitoloBlock)


    TitoloLock = display.newImage( "TitoloLock.png")
    TitoloLock .x = display.contentCenterX
    TitoloLock .y = TitoloBlock.y
    fitImage(TitoloLock ,display.actualContentWidth-90,120)
    sceneGroup:insert( TitoloLock  )

    TitoloRed = display.newImage( "TitoloRed.png")
    TitoloRed .x = TitoloBlock.x
    TitoloRed .y = TitoloBlock.y
    fitImage(TitoloRed ,display.actualContentWidth-90,120)
    sceneGroup:insert( TitoloRed )

    local optzPlay = {
        width = 1022,
        height = 192,
        defaultFile = "Play22.png",
        overFile = "Play22Over.png",
        isEnabled = true
    }

    

    Play = widget.newButton( optzPlay )
    Play.x = display.contentCenterX - display.actualContentWidth
    Play.y = display.safeScreenOriginY+250
    Play.xScale = 0.18
    Play.yScale =  0.182
    sceneGroup:insert( Play )

    local optzBoards = {
        width = 1022,
        height = 192,
        defaultFile = "BoardHome2.png",
        overFile = "BoardHome2Over.png",
        isEnabled = true
    }

    Boards = widget.newButton( optzBoards )
    Boards.x = display.contentCenterX-display.actualContentWidth
    Boards.y = Play.y+60
    Boards.xScale = 0.18
    Boards.yScale =  0.182
    sceneGroup:insert( Boards )
    
    local optzCredits = {
        width = 1022,
        height = 192,
        defaultFile = "Credits2.png",
        overFile = "Credits2Over.png",
        isEnabled = true
    }
    

    SettingsHome = widget.newButton( optzCredits )
    SettingsHome.x = display.contentCenterX-display.actualContentWidth
    SettingsHome.y = Boards.y+60
    SettingsHome.xScale = 0.18
    SettingsHome.yScale =  0.182
    sceneGroup:insert( SettingsHome )

    local fromBoard = false
    local fromPlay = false

    local function HandlerGoPlay(event)
        MyData.settings.Play = true
        MyData.settings.Board = false
        utility.saveTable(MyData.settings, "settings9.json")
        composer.gotoScene( "SelectionMod",{time = 700,effect = "fromRight",params = {fromPlay = true, fromBoard = false}})
    end 

    local function HandlerGoBoards(event)
        MyData.settings.Play = false
        MyData.settings.Board = true
        utility.saveTable(MyData.settings, "settings9.json")
        composer.gotoScene( "SelectionMod",{effect = "slideLeft", params = {fromPlay = false, fromBoard = true}} )
        
    end 

    
    Play:addEventListener( "tap", HandlerGoPlay )
    Boards:addEventListener( "tap", HandlerGoBoards )--]]
    TurnBack:addEventListener( "tap", TurnBackListener )


end

 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        --MyData.settings = utility.loadTable("settings9.json")
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        --[[transition.to(Play,{delay = 100,time = 800,x = display.contentCenterX})
        transition.to(Boards,{delay = 400,time = 800,x = display.contentCenterX})
        transition.to(SettingsHome,{delay = 700,time = 800,x = display.contentCenterX})--]]
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene