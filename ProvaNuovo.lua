local composer = require "composer"

local scene = composer.newScene()
local PassiSoluzione 
local FalgUscita = false
local objectsS 
local objects2S

function scene:create(event)
	print( "scenaNuova!!!" )
	local Gruppo = self.view
	local INITIAL1 = event.params.StringaStato

	function TuttaFunction(INITIAL)

		local hashMap = require "hash"
		local coda = require "queue"
		local Passi = {}

		tebGOAL_R = 2
		tebGOAL_C = 5
		tebHORZS = "23X"  
		tebVERTS = "BC" 
		tebLONGS = "3C" 
		tebSHORTS = "2BX" 
		tebGOAL_CAR = "X"
		tebEMPTY = "."
		tebVOID = "@"


		hashMap:__init()

		local codaVera = coda:new()


		local function at (stringa,a)  return string.sub(stringa,tonumber(a) + 1,tonumber(a) +1) end

		local function replace_char(pos, str, r)
			return str:sub(1, pos-1) .. r .. str:sub(pos+1)
		end

		function prettify(state)
			local riga1,riga2,riga3,riga4,riga5,riga6 = "","","","","",""
			for i = 1,7 do
				if i == 7 then
					riga1 = replace_char(7,riga1,"\n") 
				else   
					riga1 = replace_char(i,riga1,at(state,i-1))
				end
			end 
			for i = 7,13 do
				if i == 13 then
					riga2 = replace_char(7,riga2,"\n") 
				else   
					riga2 = replace_char(i,riga2,at(state,i-1))
				end
			end 
			for i = 13,19 do
				if i == 19 then
					riga3 = replace_char(7,riga3,"\n") 
				else   
					riga3 = replace_char(i,riga3,at(state,i-1))
				end
			end 
			for i = 19,25 do
				if i == 25 then
					riga4 = replace_char(7,riga4,"\n") 
				else   
					riga4 = replace_char(i,riga4,at(state,i-1))
				end
			end
			for i = 25,31 do
				if i == 31 then
					riga5 = replace_char(7,riga5,"\n") 
				else   
					riga5 = replace_char(i,riga5,at(state,i-1))
				end
			end 
			for i = 31,36 do  
				riga6 = replace_char(i,riga6,at(state,i-1))
			end 

			local stateTorna = riga1..riga2..riga3..riga4..riga5..riga6

			return stateTorna 
		end


		function rc2i(r,c )
			local torna = r*6 + c
			return torna
		end

		function isType(entity,type)
			local flag = -1
			for i = 0,string.len(type)-1  do
				if at(type,i) == entity then
					flag = 2
					break
				end
			end
			local ritornoFlag = flag ~= -1
			return ritornoFlag

		end


		function length(car)
			local ritorno 
			if isType(car,tebLONGS) then
				ritorno = 3
			elseif isType(car,tebSHORTS) then
				ritorno = 2
			end

			return ritorno 
		end

		function inBound( v,max )

			return (v>=0) and (v<max)
		end

		function at2(state,r,c)
			local torna
			if (inBound(r,6) and inBound(c,6)) then
				torna = at(state,rc2i(r,c))
			else
				torna = tebVOID
			end 
			return torna
		end

		function isGoal(state)

			return (at2(state,tebGOAL_R,tebGOAL_C) == tebGOAL_CAR)
		end

		function countSpaces(state,r,c,dr,dc)
			local k = 0
			while (at2(state,r + (k*dr),c + (k*dc)) == tebEMPTY ) do
				k = k+1
			end
			return k
		end



		function propose(next,prev)
			local prev2 = prev
			if prev2 == nil then
				prev = "Null"
			end
			if not hashMap:contains(next) then
				--print( "propose!!" )
				hashMap:add(next,prev)
				codaVera:push(next)
			end
		end



		function trace(current )
			local prev = hashMap:get(current)
			local step
			if prev == "Null" then
				step = 0
			else
				step = trace(prev) + 1
			end
			table.insert( Passi, current )
			return step    
		end




		function slide(current,r,c,type,distance,dr,dc,n)
			r = r + distance*dr
			c = c +distance*dc
			local car = at2(current,r,c)
			if not isType(car,type) then

			else
				local L = length(car)
				local sb = ""
				sb = current
			  --print( sb )
			  for j =0,n-1 do
				r = r-dr
				c = c-dc
				sb = replace_char(rc2i(r, c)+1,sb, car)
				sb = replace_char(rc2i(r + L * dr, c + L * dc)+1,sb,tebEMPTY)
				propose(sb,current)
			  end
			  car = nil
			  L = nil
			  sb = nil
		  end
		end



		function explore1(current)

			for i = 0,5 do
				for c = 0,5 do
					if at2(current,i,c) ~= tebEMPTY then 
					else
						local nU = countSpaces(current, i, c, -1, 0)
						local nD = countSpaces(current, i, c, 1, 0)
						local nL = countSpaces(current, i, c, 0, -1)
						local nR = countSpaces(current, i, c, 0, 1)
						slide(current, i, c, tebVERTS, nU, -1, 0, nU + nD - 1)
						slide(current, i, c, tebVERTS, nD, 1, 0, nU + nD - 1)
						slide(current, i, c, tebHORZS, nL, 0, -1, nL + nR - 1)
						slide(current, i, c, tebHORZS, nR, 0, 1, nL + nR - 1)
					end
				end
			end
		end

		function lancia(INITIAL)
			--print( INITIAL )
			propose(INITIAL,nil)
			local solved = false
			while not codaVera:is_empty() do
				local current = codaVera:pop()
				if isGoal(current) and not solved then
					print( "solved" )
					solved = true
					trace(current)
					break
				end
				explore1(current)
			end
		end

		lancia(INITIAL)
		hashMap = nil
		codaVera = nil
		collectgarbage()


		return Passi
	end

	PassiSoluzione = TuttaFunction(INITIAL1)

	local function atS (stringa,a)  return string.sub(stringa,tonumber(a),tonumber(a)) end


	local function Esegui(tabellaStringa1,tabellaStringa2,indiceU)

		objectsS = {}
		objects2S = {}
		local grid = {}
		for i =1,6 do
			grid[i] = {}
		end

		local grid2 = {}
		for i =1,6 do
			grid2[i] = {}
		end

		local grid3 = {}
		for i = 1,6 do 
			grid3[i] = {}
		end


		local GRID_WIDTH_PX = display.actualContentWidth-25
		local GRID_HEIGHT_PX = GRID_WIDTH_PX

		local CELL_WIDTH_V = GRID_WIDTH_PX/6
		local CELL_HEIGHT_V = GRID_HEIGHT_PX/3

		local CELL_WIDTH_O = GRID_WIDTH_PX/3
		local CELL_HEIGHT_O = GRID_HEIGHT_PX/6

		local CELL_WIDTH_V3 = GRID_WIDTH_PX/6
		local CELL_HEIGHT_V3 = GRID_HEIGHT_PX/2

		local CELL_WIDTH_O3 = GRID_WIDTH_PX/2
		local CELL_HEIGHT_O3 = GRID_HEIGHT_PX/6

		checkerBoard = display.newRoundedRect(Gruppo,display.contentCenterX,display.contentCenterY,GRID_WIDTH_PX, GRID_HEIGHT_PX,20)
		checkerBoard.alpha = 0

		local gbOffsetX = checkerBoard.x - ( checkerBoard.width * checkerBoard.anchorX ) 
		local gbOffsetY = checkerBoard.y - ( checkerBoard.height * checkerBoard.anchorY )


		local LimitatoriY = {}
		for i = 1,6 do
			LimitatoriY[i] = {}
		end

		local LimitatoriX = {}
		for i = 1,6 do
			LimitatoriX[i] = {}
		end


		l =1
		r = 1
		for i =1,6 do
			for j = 1,6 do
				local XPos,Ypos
				cell = display.newRoundedRect(Gruppo, 0,0,CELL_WIDTH_V,CELL_WIDTH_V,6)
				cell.x = (j-1) *CELL_WIDTH_V + (CELL_WIDTH_V*0.5) +gbOffsetX
				cell.y = (i-1) * CELL_WIDTH_V + (CELL_WIDTH_V*0.5) + gbOffsetY
				cell.alpha = 0
				--cell:setFillColor( 0,0,0 )
				grid3[i][j]=cell
				if l<7 then
					LimitatoriX[l].X = cell.width
					LimitatoriX[l].Centro = cell.x
				end 
				l = l+1
			end
			LimitatoriY[i].Y = cell.height
			LimitatoriY[i].Centro = cell.y
		end



		ZonaLimitatoriY = {}
		for i =1,6 do
			ZonaLimitatoriY[i] = display.newRect( Gruppo,checkerBoard.x, LimitatoriY[i].Centro, checkerBoard.width, LimitatoriY[i].Y )
			ZonaLimitatoriY[i]:setFillColor( 0,0,0 )
			ZonaLimitatoriY[i].alpha = 0
		end

		ZonaLimitatoriX = {}
		for i =1,6 do
			ZonaLimitatoriX[i] = display.newRect(Gruppo,LimitatoriX[i].Centro, checkerBoard.y, LimitatoriX[i].X, checkerBoard.height )
			ZonaLimitatoriX[i]:setFillColor( 1,1,1 )
			ZonaLimitatoriX[i].alpha = 0
		end


		function StampaQuadratiDoveMiTrovoXVers2(rettangolo)
			local ZonaXMin, ZonaXMax
			if (rettangolo.What == "Vert2" or rettangolo.What == "Vert3" ) then
				if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[1].contentBounds.xMax ) then
					ZonaXmin = 1
					ZonaXMax = 1
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
					ZonaXmin = 2
					ZonaXMax = 2
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax) then
					ZonaXmin = 3
					ZonaXMax = 3
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax) then
					ZonaXmin = 4
					ZonaXMax = 4
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
					ZonaXmin = 5
					ZonaXMax = 5
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax) then
					ZonaXmin = 6
					ZonaXMax = 6
				end

			elseif (rettangolo.What == "Orizz2") then
				if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
					ZonaXmin = 1
					ZonaXMax = 2
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
					ZonaXmin = 2
					ZonaXMax = 3
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
					ZonaXmin = 3
					ZonaXMax = 4
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
					ZonaXmin = 4
					ZonaXMax = 5
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
					ZonaXmin = 5
					ZonaXMax = 6
				end

			elseif (rettangolo.What == "Orizz3") then
				if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
					ZonaXmin = 1
					ZonaXMax = 3
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
					ZonaXmin = 2
					ZonaXMax = 4
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
					ZonaXmin = 3
					ZonaXMax = 5
				elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
					ZonaXmin = 4
					ZonaXMax = 6
				end

			end
			return ZonaXmin,ZonaXMax
		end




		function StampaQuadratiDoveMiTrovoYVers2(rettangolo)
			local ZonaYMin, ZonaYMax 
			if (rettangolo.What == "Vert2") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
					ZonaYMin =1
					ZonaYMax = 2 
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =2
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =5
					ZonaYMax = 6
				end
			elseif (rettangolo.What == "Vert3") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =1
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax) then
					ZonaYMin =2
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 6
				end
			elseif (rettangolo.What == "Orizz2" or rettangolo.What == "Orizz3") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[1].contentBounds.yMax) then
					ZonaYMin =1
					ZonaYMax = 1
				elseif(rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
					ZonaYMin =2
					ZonaYMax = 2
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =5
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =6
					ZonaYMax = 6
				end

			end
			return ZonaYMin,ZonaYMax
		end

		k =1
		j = 1
		for riga =1,6 do
			for colonna = 1,6 do
				if(atS(tabellaStringa1,k) == 'B' and grid[riga][colonna] == nil ) then
					piece = display.newRoundedRect(Gruppo,0,0, CELL_WIDTH_V, CELL_HEIGHT_V,6 )
					piece.x = (colonna - 1) * CELL_WIDTH_V + (CELL_WIDTH_V * 0.5) + gbOffsetX
					piece.y = (riga) * CELL_HEIGHT_O + gbOffsetY 
					piece.xScale = 0.93
					piece.yScale = 0.95
					piece.alpha = 0.01
					--piece:setFillColor(0.13,0.29,0.41)
					piece.MyID = j
					piece.What = "Vert2"
					--piece.fill = paint
					grid[riga][colonna] = piece
					grid[riga+1][colonna] = piece
					objectsS[j] = piece
					j = j+1


				elseif(atS(tabellaStringa1,k) == 'C' and grid[riga][colonna] == nil ) then
					piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_V3, CELL_HEIGHT_V3,6 )
					piece.x = (colonna - 1) * CELL_WIDTH_V3+ (CELL_WIDTH_V3 * 0.5) + gbOffsetX
					piece.y = (riga ) * CELL_HEIGHT_O + (CELL_WIDTH_V3 * 0.5)+gbOffsetY 
					piece.xScale = 0.93
					piece.yScale = 0.97
					piece.alpha = 0.01
						--piece:setFillColor( 0.13,0.29,0.41)
						piece.MyID = j
						piece.What = "Vert3"
						--piece.fill = paint
						grid[riga][colonna] = piece
						grid[riga+1][colonna] = piece
						grid[riga+2][colonna] = piece
						objectsS[j] = piece
						j = j+1
						--piece:addEventListener("touch",onTouchV)	

					elseif(atS(tabellaStringa1,k) == '2' and grid[riga][colonna] == nil) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
						piece.xScale = 0.95
						piece.yScale = 0.90
						piece.alpha = 0.01
						--piece:setFillColor(0.13,0.29,0.41)
						piece.MyID = j
						piece.What = "Orizz2"
						
						grid[riga][colonna] = piece
						grid[riga][colonna+1] = grid[riga][colonna]
						objectsS[j] = piece
						j = j+1
						--piece:addEventListener("touch",onTouchO)	

					elseif(atS(tabellaStringa1,k) == '3' and grid[riga][colonna] == nil) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O3, CELL_HEIGHT_O3,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O3 + (CELL_WIDTH_O3 * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O3 + (CELL_HEIGHT_O3 * 0.5) + gbOffsetY 
						piece.xScale = 0.97
						piece.yScale = 0.92
						piece.alpha = 0.01
						--piece:setFillColor(0.13,0.29,0.41)
						piece.MyID = j
						piece.What = "Orizz3"
						grid[riga][colonna] = piece
						grid[riga][colonna+1] = grid[riga][colonna]
						grid[riga][colonna+2] = grid[riga][colonna]
						objectsS[j] = piece
						j = j+1
						--piece:addEventListener("touch",onTouchO)	

					elseif (atS(tabellaStringa1,k) == 'X'and grid[riga][colonna] == nil ) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
						piece.xScale = 0.95
						piece.yScale = 0.90
						piece:setFillColor( 0.90, 0.22, 0.22)
						piece.alpha = 0.01
						piece.MyID = j
						piece.What = "Orizz2"
						piece.Red = "Red"
						
						grid[riga][colonna] = piece
						grid[riga][colonna+1] = grid[riga][colonna]
						objectsS[j] = piece
						piece.isToucheEnable = true
						--Rossa = j
						j = j+1
						--piece:addEventListener("touch",onTouchOX)				

					end
					k = k+1
				end
			end




			k =1
			j = 1
			for riga =1,6 do
				for colonna = 1,6 do

					if(atS(tabellaStringa2,k) == 'B' and grid2[riga][colonna] == nil ) then
						piece = display.newRoundedRect(Gruppo,0,0, CELL_WIDTH_V, CELL_HEIGHT_V,6 )
						piece.x = (colonna - 1) * CELL_WIDTH_V + (CELL_WIDTH_V * 0.5) + gbOffsetX
						piece.y = (riga) * CELL_HEIGHT_O + gbOffsetY 
						piece.xScale = 0.93
						piece.yScale = 0.95
						piece.alpha = 0.01
    					--piece:setFillColor(0.13,0.29,0.41)
    					piece.MyID = j
    					piece.What = "Vert2"
    					--piece.fill = paint
    					grid2[riga][colonna] = piece
    					grid2[riga+1][colonna] = piece
    					objects2S[j] = piece
    					j = j+1


    				elseif(atS(tabellaStringa2,k) == 'C' and grid2[riga][colonna] == nil ) then
    					piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_V3, CELL_HEIGHT_V3,6 )
    					piece.x = (colonna - 1) * CELL_WIDTH_V3+ (CELL_WIDTH_V3 * 0.5) + gbOffsetX
    					piece.y = (riga ) * CELL_HEIGHT_O + (CELL_WIDTH_V3 * 0.5)+gbOffsetY 
    					piece.xScale = 0.93
    					piece.yScale = 0.97
    					piece.alpha = 0.01
    						--piece:setFillColor( 0.13,0.29,0.41)
    						piece.MyID = j
    						piece.What = "Vert3"
    						--piece.fill = paint
    						grid2[riga][colonna] = piece
    						grid2[riga+1][colonna] = piece
    						grid2[riga+2][colonna] = piece
    						objects2S[j] = piece
    						j = j+1
    						--piece:addEventListener("touch",onTouchV)	

					elseif(atS(tabellaStringa2,k) == '2' and grid2[riga][colonna] == nil) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
						piece.xScale = 0.95
						piece.yScale = 0.90
						piece.alpha = 0.01
						--piece:setFillColor(0.13,0.29,0.41)
						piece.MyID = j
						piece.What = "Orizz2"
						
						grid2[riga][colonna] = piece
						grid2[riga][colonna+1] = grid2[riga][colonna]
						objects2S[j] = piece
						j = j+1
						--piece:addEventListener("touch",onTouchO)	

					elseif(atS(tabellaStringa2,k) == '3' and grid2[riga][colonna] == nil) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O3, CELL_HEIGHT_O3,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O3 + (CELL_WIDTH_O3 * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O3 + (CELL_HEIGHT_O3 * 0.5) + gbOffsetY 
						piece.xScale = 0.97
						piece.yScale = 0.92
						piece.alpha = 0.01
						--piece:setFillColor(0.13,0.29,0.41)
						piece.MyID = j
						piece.What = "Orizz3"
						grid2[riga][colonna] = piece
						grid2[riga][colonna+1] = grid2[riga][colonna]
						grid2[riga][colonna+2] = grid2[riga][colonna]
						objects2S[j] = piece
						j = j+1
						--piece:addEventListener("touch",onTouchO)	

					elseif (atS(tabellaStringa2,k) == 'X'and grid2[riga][colonna] == nil ) then
						piece = display.newRoundedRect(Gruppo, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
						piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
						piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
						piece.xScale = 0.95
						piece.yScale = 0.90
						piece:setFillColor( 0.90, 0.22, 0.22)
						piece.alpha = 0.01
						piece.MyID = j
						piece.What = "Orizz2"
						piece.Red = "Red"
						
						grid2[riga][colonna] = piece
						grid2[riga][colonna+1] = grid2[riga][colonna]
						objects2S[j] = piece
						
						--Rossa = j
						j = j+1
						--piece:addEventListener("touch",onTouchOX)				

					end
					k = k+1
				end
			end



			function EsisteUno(xMinI,xMaxI,yMinI,yMaxI,What,j)
				local Flag = false
				local k = j 
				for i=1,#objects2S do
					local xMin2,xMax2 = StampaQuadratiDoveMiTrovoXVers2(objects2S[i])
					local yMin2,yMax2 = StampaQuadratiDoveMiTrovoYVers2(objects2S[i])
					if xMinI == xMin2 and xMaxI == xMax2 and yMinI == yMin2 and yMaxI == yMax2 then
						Flag = true
						objects2S[i].alpha = 0
						table.remove( objects2S,i )
						break
					elseif What == "Orizz2" and yMinI == yMin2 and yMaxI == yMax2 and objects2S[i].What == "Orizz2" then
						Passi1[k].xMin2 = xMin2
						Passi1[k].xMax2 = xMax2
						Passi1[k].yMin2 = yMin2
						Passi1[k].yMax2 = yMax2

					break
				elseif What == "Orizz3" and yMinI == yMin2 and yMaxI == yMax2 and objects2S[i].What == "Orizz3" then 
					Passi1[k].xMin2 = xMin2
					Passi1[k].xMax2 = xMax2
					Passi1[k].yMin2 = yMin2
					Passi1[k].yMax2 = yMax2
					break
				elseif What == "Vert2" and xMinI == xMin2 and xMaxI == xMax2 and objects2S[i].What == "Vert2" then 
					Passi1[k].xMin2 = xMin2
					Passi1[k].xMax2 = xMax2
					Passi1[k].yMin2 = yMin2
					Passi1[k].yMax2 = yMax2
					break
				elseif What == "Vert3" and xMinI == xMin2 and xMaxI == xMax2 and objects2S[i].What == "Vert3" then 
					Passi1[k].xMin2 = xMin2
					Passi1[k].xMax2 = xMax2
					Passi1[k].yMin2 = yMin2
					Passi1[k].yMax2 = yMax2
					break
				end
			end
			return Flag
		end


        
		local Originale
		for i =1,#objectsS do
			local xMinI,xMaxI = StampaQuadratiDoveMiTrovoXVers2(objectsS[i])
			local yMinI,yMaxI  = StampaQuadratiDoveMiTrovoYVers2(objectsS[i])
			if not EsisteUno(xMinI,xMaxI,yMinI,yMaxI,objectsS[i].What,indiceU) then
				print("Vecchio: "..xMinI.." "..xMaxI.." "..yMinI.." "..yMaxI )
				Passi1[indiceU].xMin1 = xMinI
				Passi1[indiceU].xMax1 = xMaxI
				Passi1[indiceU].yMin1 = yMinI
				Passi1[indiceU].yMax1 = yMaxI
				break
			end
		end
		
		for ik = 1,#objectsS do
			objectsS[ik].alpha = 0 
		end
		for ik2 = 1,#objects2S do
			objects2S[ik2].alpha = 0
		end
	end --ESEGUI


	for u = 1,#PassiSoluzione-1 do
		--print("Passo: "..u )
		Esegui(PassiSoluzione[u],PassiSoluzione[u+1],u)
		if u == #PassiSoluzione-1 then
			--[[for indiceObjS = 1,#objectsS do
				objectsS[indiceObjS].alpha = 0

			end
			print("Bella"..objectsS[2].alpha)--]]
			FalgUscita = true 
			
		end
	end

	
end






function enterFrameSolutore(event)
	print( "frameSolutore" )
	if FalgUscita then
		composer.hideOverlay()
	end
end

function scene:show(event)
	local phase = event.phase
	if ( phase == "will" ) then
		Runtime:addEventListener("enterFrame", enterFrameSolutore)
	elseif ( phase == "did" ) then
	--print(event.params)
end
end

function scene:hide( event )
	local phase = event.phase
	local parent = event.parent 
	if ( phase == "will" ) then
	--parent:resumeAfterOverlay()
	--audio.fadeOut( { time = 1000 })
elseif ( phase == "did" ) then
	Runtime:removeEventListener("enterFrame", enterFrameSolutore)
	  --print("HIDE: "..hide)
  end
end

function scene:destroy( event )
  --composer.removeScene( current,false )
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")
return scene