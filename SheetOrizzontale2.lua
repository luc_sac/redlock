--
-- created with TexturePacker - https://www.codeandweb.com/texturepacker
--
-- $TexturePacker:SmartUpdate:6c73da2cd55bb95bfddb29020e03f5ee:81b112da26124ed7666ff11445facd87:08cba1e171a6753f326da2b212312827$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1
            x=1,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 2
            x=1486,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 3
            x=1189,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 4
            x=1,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 5
            x=298,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 6
            x=1486,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 7
            x=1189,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 8
            x=892,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 9
            x=595,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 10
            x=298,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 11
            x=595,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 12
            x=892,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 13
            x=1189,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 14
            x=1486,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 15
            x=1,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 16
            x=298,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 17
            x=595,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 18
            x=892,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 19
            x=1189,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 20
            x=1,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 21
            x=298,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 22
            x=595,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 23
            x=892,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 24
            x=1189,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 25
            x=1486,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 26
            x=1,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 27
            x=298,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 28
            x=595,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 29
            x=892,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 30
            x=1486,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 31
            x=595,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 32
            x=298,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 33
            x=1,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 34
            x=1486,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 35
            x=1189,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 36
            x=892,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 37
            x=595,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 38
            x=298,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 39
            x=1,
            y=301,
            width=295,
            height=148,

        },
        {
            -- 40
            x=1189,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 41
            x=892,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 42
            x=595,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 43
            x=298,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 44
            x=1,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 45
            x=1486,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 46
            x=1189,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 48
            x=892,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 49
            x=595,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 50
            x=298,
            y=1,
            width=295,
            height=148,

        },
        {
            -- 51
            x=595,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 52
            x=892,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 53
            x=1189,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 54
            x=1486,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 55
            x=298,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 56
            x=1,
            y=601,
            width=295,
            height=148,

        },
        {
            -- 57
            x=1189,
            y=451,
            width=295,
            height=148,

        },
        {
            -- 58
            x=1486,
            y=151,
            width=295,
            height=148,

        },
        {
            -- 59
            x=1,
            y=1,
            width=295,
            height=148,

        },
    },

    sheetContentWidth = 1782,
    sheetContentHeight = 750
}

SheetInfo.frameIndex =
{

    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["10"] = 10,
    ["11"] = 11,
    ["12"] = 12,
    ["13"] = 13,
    ["14"] = 14,
    ["15"] = 15,
    ["16"] = 16,
    ["17"] = 17,
    ["18"] = 18,
    ["19"] = 19,
    ["20"] = 20,
    ["21"] = 21,
    ["22"] = 22,
    ["23"] = 23,
    ["24"] = 24,
    ["25"] = 25,
    ["26"] = 26,
    ["27"] = 27,
    ["28"] = 28,
    ["29"] = 29,
    ["30"] = 30,
    ["31"] = 31,
    ["32"] = 32,
    ["33"] = 33,
    ["34"] = 34,
    ["35"] = 35,
    ["36"] = 36,
    ["37"] = 37,
    ["38"] = 38,
    ["39"] = 39,
    ["40"] = 40,
    ["41"] = 41,
    ["42"] = 42,
    ["43"] = 43,
    ["44"] = 44,
    ["45"] = 45,
    ["46"] = 46,
    ["48"] = 47,
    ["49"] = 48,
    ["50"] = 49,
    ["51"] = 50,
    ["52"] = 51,
    ["53"] = 52,
    ["54"] = 53,
    ["55"] = 54,
    ["56"] = 55,
    ["57"] = 56,
    ["58"] = 57,
    ["59"] = 58,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
